import { Avatar, Checkbox } from "@material-ui/core";
import React from "react";

const User = ({ firstName, lastName, avatar }) => {
  return (
    <div className="user-container">
      <div className="user-row">
        <div className="user-avatar">
          <Avatar alt={firstName} src={avatar} />
        </div>
        <div className="user-name">
          <p>
            {firstName} {lastName}
          </p>
        </div>
      </div>
      <div className="user-checkbox">
        <Checkbox color="primary" />
      </div>
    </div>
  );
};

export default User;
