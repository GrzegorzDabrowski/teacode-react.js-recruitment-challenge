import React, { useState, useEffect } from "react";
import axios from "axios";
import SearchIcon from "@material-ui/icons/Search";
import "./scss/main.scss";
import NavBar from "./components/NavBar";
import User from "./components/User";

function App() {
  const [users, setUsers] = useState([]);
  const [search, setSearch] = useState("");

  useEffect(() => {
    axios
      .get(
        "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json"
      )
      .then((responce) => {
        setUsers(responce.data);
      })
      .catch((error) => console.log(error));
  }, []);

  const handleChange = (e) => {
    setSearch(e.target.value);
  };

  const filteredUsers = users.filter(
    (user) =>
      user.first_name.toLowerCase().includes(search.toLowerCase()) ||
      user.last_name.toLowerCase().includes(search.toLowerCase())
  );

  return (
    <div className="App">
      <NavBar />
      <div className="container">
        <div className="user-search">
          <SearchIcon />
          <input
            type="text"
            placeholder="Search name"
            onChange={handleChange}
          />
        </div>
        {filteredUsers.map((user) => {
          return (
            <User
              key={user.id}
              users={users}
              firstName={user.first_name}
              lastName={user.last_name}
              avatar={user.avatar}
            />
          );
        })}
      </div>
    </div>
  );
}

export default App;
